package hu.idomsoft.zookeeper.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Taini on 2017.02.21..
 */
@Configuration
@EnableFeignClients
@EnableDiscoveryClient
public class ExampleClient {

    @Autowired
    private TheClient theClient;

    /**
     * Create a REST-ful client to connect to the "diner" service available on
     * ZooKeeper.
     *
     */
    @FeignClient(name = "diner")
    interface TheClient {
        @RequestMapping(value = "/example/isopen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        ExampleClientResponse isOpen(@RequestBody ExampleClientRequest request);
    }

    /**
     * Initiate call to diner.
     *
     * @param request
     * @return the response
     */
    public ExampleClientResponse testDay(ExampleClientRequest request) {
        return theClient.isOpen(request);
    }

}
