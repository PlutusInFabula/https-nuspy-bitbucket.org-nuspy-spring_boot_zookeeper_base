package hu.idomsoft.zookeeper.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Taini on 2017.02.21..
 */
@RestController
public class ExampleController {

    private static final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @Autowired
    private ExampleClient dinerClient;

    /**
     * This service calls the diner service to find if the diner is open on given day.
     *
     * @param request
     * @return the response received from the invocation to diner
     */
    @RequestMapping(path = "/example/myexample", method= RequestMethod.POST)
    public ExampleClientResponse findDinerStatus(@RequestBody ExampleClientRequest request) {
        logger.info("findDinerStatus::day=" + request.getDay());
        return dinerClient.testDay(request);
    }
}
