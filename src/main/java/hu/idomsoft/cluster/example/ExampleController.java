package hu.idomsoft.cluster.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Taini on 2017.02.21..
 */
@RestController
public class ExampleController {

    private static final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @RequestMapping(path = "/example/isopen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ExampleResponse isOpen(@RequestBody(required = false) ExampleRequest exampleRequest) {
        final String day = exampleRequest.getDay();
        logger.info("Diner day = " + day);
        String status = "open";
        // The diner is open on all days except Sunday
        if ("1".equalsIgnoreCase(day)) {
            status = "close";
        }
        final ExampleResponse response = new ExampleResponse();
        response.setStatus(status);
        return response;
    }

}
