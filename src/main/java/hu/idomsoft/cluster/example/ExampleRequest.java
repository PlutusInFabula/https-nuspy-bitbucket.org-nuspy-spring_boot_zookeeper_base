package hu.idomsoft.cluster.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Taini on 2017.02.21..
 */

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExampleRequest {

    /**The Day of Week, 1 = Sunday, 2, 3, ...**/
    private String day;

}
