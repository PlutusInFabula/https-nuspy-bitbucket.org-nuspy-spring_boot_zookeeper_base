package hu.idomsoft.cluster.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Taini on 2017.02.21..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ExampleResponse {
    /**open or closed**/
    private String status;
}
