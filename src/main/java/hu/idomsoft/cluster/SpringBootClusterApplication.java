package hu.idomsoft.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringBootClusterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootClusterApplication.class, args);
	}
}
